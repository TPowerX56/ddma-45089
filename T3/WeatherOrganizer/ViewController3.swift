//
//  ViewController3.swift
//  WeatherOrganizer
//
//  Created by PowerX56 on 7/30/17.
//  Copyright © 2017 PowerX56. All rights reserved.
//

import UIKit

class eventos: UITableViewCell {
    
    @IBOutlet weak var horasEvento: UILabel!
    @IBOutlet weak var tituloEvento: UILabel!
    @IBOutlet weak var iconeEvento: UIImageView!
}

class ViewController3: UIViewController,UITableViewDelegate,UITableViewDataSource {

    //Variaveis que vao receber o dia,mes e ano que o utilizador selecionou no calendario
    var anoS : Int = 0
    var mesS : Int = 0
    var diaS : Int = 0
    var accaoS : Int = 0
    var ImStartingToGetItNow : Int = 0
    
    @IBOutlet weak var tituloItem: UINavigationItem!
    
    @IBOutlet weak var tabela2: UITableView!
    
    //arrays Informacao do Evento
    var ArrayEventosPoderoso : [String] = []
    var ArrayTratamento:[(ano: Int, mes: Int, dia: Int, hora: Int, minutos: Int, desc: String, photo: String)] = []
    
    let defaults = UserDefaults.standard
    
    //Carregamento de dados de eventos e atualizacao do titulo
    override func viewDidLoad() {
        super.viewDidLoad()
        ArrayEventosPoderoso = defaults.array(forKey: "eventos")  as? [String] ?? [String]()
        tituloItem.title =  String(diaS) + "/" + String(mesS) + "/" + String(anoS) + " - Eventos";
        loadData()
    }
    
    //neste prepararCalendario e feito o filtro de apenas queremos os dados que correspondem ao dia,mes e ano selecionado
    func loadData() {
        ArrayTratamento.removeAll()
        if ArrayEventosPoderoso.count > 0 {
            var i = 0
            while i < ArrayEventosPoderoso.count-1 {
                if ArrayEventosPoderoso[i] == String(anoS) && ArrayEventosPoderoso[i+1] == String(mesS) && ArrayEventosPoderoso[i+2] == String(diaS){
                            ArrayTratamento.append((ano: Int(ArrayEventosPoderoso[i])!,mes: Int(ArrayEventosPoderoso[i+1])!,dia: Int(ArrayEventosPoderoso[i+2])!, hora: Int(ArrayEventosPoderoso[i+3])!,minutos: Int(ArrayEventosPoderoso[i+4])!,desc: ArrayEventosPoderoso[i+5],photo: ArrayEventosPoderoso[i+6]))
                }
                i = i + 7
            }
        }
        
        //e realizado um Sort, para apresentar ao utilizador os eventos que tem nesse dia, ordenados da menor hora para a maior hora, e caso sejam da mesma hora existe o desampate atraves dos minutos
        ArrayTratamento.sort { $0.3 == $1.3 ? $0.4 < $1.4 : $0.3 < $1.3 }
    }

    //o utilizador ao voltar para tras, fecha a modal View e muda rapidamente a Tab SelectedIndex para 1, o calendario
    @IBAction func backbutton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
        if let tabBarController = self.presentingViewController as? UITabBarController {
            tabBarController.selectedIndex = 1
        }
    }
    
    //e utilizado a materia de passar dados para outra View, para adicionar um novo evento a que dia mes e ano pertencem
    @IBAction func addbutton(_ sender: Any) {
        accaoS = 0
        performSegue(withIdentifier: "segue2", sender: self)
    }
    
    //depois de ser feita o tratamento dos dados, ja temos os eventos que pertencem ao dia,mes e ano respetivamente ordenados
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ArrayTratamento.count
    }
    
    //apresenta os dados na TableView
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell3") as! eventos
        cell.tituloEvento.text = ArrayTratamento[indexPath.row].desc
        
        var minutosFix2 : String = ""
        
        //mais uma vez, se os minutos so tiverem um digito, e acrecentado um zero antes
        let minutosFix : Int = ArrayTratamento[indexPath.row].minutos
        if minutosFix < 10 {
            minutosFix2 = "0" + String(minutosFix)
        }else{
            minutosFix2 = String(minutosFix)
        }
        cell.horasEvento.text = String(ArrayTratamento[indexPath.row].hora) + ":" + minutosFix2
        //o nome da imagem corresponde ao nome do ficheiro da imagem
        cell.iconeEvento.image = UIImage(named: ArrayTratamento[indexPath.row].photo)
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue2" {
            if let toViewController = segue.destination as? ViewController4 {
                toViewController.anoS2 = anoS
                toViewController.mesS2 = mesS
                toViewController.diaS2 = diaS
                toViewController.TipoAccao = accaoS
                toViewController.ImStartingToGetItLel = ImStartingToGetItNow
            }
        }
    }
    
    //atualizar os dados automaticamente caso o utilizador venha da atividade de adicionar um novo evento
    override func viewDidAppear(_ animated: Bool) {
        ArrayEventosPoderoso = defaults.array(forKey: "eventos")  as? [String] ?? [String]()
        loadData()
        tabela2.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let TituloGET : String = String(ArrayTratamento[indexPath.row].desc)
        let horaGET : Int = Int(ArrayTratamento[indexPath.row].hora)
        let minutoGET : Int = Int(ArrayTratamento[indexPath.row].minutos)
        let imageGET : String = String(ArrayTratamento[indexPath.row].photo)
        
            var i = 0
            while i < ArrayEventosPoderoso.count-1 {
                if ArrayEventosPoderoso[i] == String(anoS) && ArrayEventosPoderoso[i+1] == String(mesS) && ArrayEventosPoderoso[i+2] == String(diaS) && ArrayEventosPoderoso[i+3] == String(horaGET) && ArrayEventosPoderoso[i+4] == String(minutoGET) && ArrayEventosPoderoso[i+5] == String(TituloGET) && ArrayEventosPoderoso[i+6] == String(imageGET){
                    ImStartingToGetItNow = i
                }
                i = i + 7
            }
        accaoS = 1
        performSegue(withIdentifier: "segue2", sender: self)
    }
    
}
