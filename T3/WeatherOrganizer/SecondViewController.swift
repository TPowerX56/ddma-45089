//
//  SecondViewController.swift
//  WeatherOrganizer
//
//  Created by PowerX56 on 7/29/17.
//  Copyright © 2017 PowerX56. All rights reserved.
//

import UIKit

//utilizacao da materia collectionView para um exemplo de Calendario
class CalendarioColection: UICollectionViewCell {
    @IBOutlet weak var certo: UIImageView!
    @IBOutlet weak var dia: UILabel!
}

    //variaveis mes e ano, que vao mudando conforme o mes e o ano selecionado
    var mes : Int = 6
    var ano : Int = 2017
    //variavel que vai ser alterada quando o utilizador clica em uma celula da collection View para depois ser enviada durante o Segue
    var diasend : Int = 0
    //arrayDias para os dias de cada mes, pois meses diferentes podem ter um total de dias diferente
    var arraydias : [Int] = []

class SecondViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var colection: UICollectionView!
    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var botao1: UIButton!
    @IBOutlet weak var botao2: UIButton!
    
    //mais uma vez os arrays
    var ArrayEventosPoderoso : [String] = []
    var ArrayTratamento:[(ano: Int, mes: Int, dia: Int, hora: Int, minutos: Int, desc: String, photo: String)] = []
    
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ArrayEventosPoderoso = defaults.array(forKey: "eventos")  as? [String] ?? [String]()
        let date = Date()
        let calendar = Calendar.current
        //o utilizador comeca por predefinicao, no mes e ano atuais no calendario quando e apresentado a View
        ano = calendar.component(.year, from: date)
        mes = calendar.component(.month, from: date)
        LittleUpdate()
        killdemall()
        prepararCalendario()
    }
    
    //a collectionView e preenchida conforme os dias que existem no mes
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arraydias.count
    }

    //a collection view e Preenchida com valores simples,um para cada vez, e ainda e apresentado um Checked caso tenha marcado um evento para esse dia
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell2", for: indexPath as IndexPath) as! CalendarioColection
        cell.dia.text = String(indexPath.row + 1)
        var checkData : Int = 0
        if ArrayTratamento.count > 0 {
            for i in 0...ArrayTratamento.count-1 {
                if Int(ArrayTratamento[i].dia) == indexPath.row + 1 {
                    checkData = 1
                }
            }
        }

        if checkData == 1 {
            cell.certo.image = #imageLiteral(resourceName: "check")
        }else{
            cell.certo.image = nil
        }
        
        return cell
    }
    
    //botao esquerdo, simples para andar para tras nos meses e caso seja mes 1, anda para tras 1 ano
    @IBAction func botao1click(_ sender: Any) {
        if mes > 1 {
            mes = mes - 1
        }else {
            mes = 12
            ano = ano - 1
        }
        prepararCalendario()
        LittleUpdate()
        killdemall()
    }
    
    //botao direito, simples para andar para frente nos meses e caso seja mes 12, anda para a frente 1 ano
    @IBAction func botao2click(_ sender: Any) {
        if mes < 12 {
            mes = mes + 1
        }else{
            ano = ano + 1
            mes = 1
        }
        prepararCalendario()
        LittleUpdate()
        killdemall()
    }
    
    //apenas a atualizacao do titulo da Navigation Bar
    func LittleUpdate() {
        titulo.text = "Calendario - " + String(mes) + "/" + String(ano)
    }
    
    //quando o utilizador faz a mudanca do mes, temos de verificar o numero de dias desse mes, caso seja fevereiro ainda e necessario necessario verificar se estamos em um LeapYear que faz diferenca se o mes tem 29 ou 28 dias, e depois e feita o recarregamento de dados da collectionView
    func killdemall() {
        var destino : Int = 0
        
        arraydias.removeAll()
        if mes == 2 {
            if checkIfLeapYear(year: ano) == 1 {
                destino = 29
            }else {
                destino = 28
            }
        }else if mes <= 7 {
            if mes % 2 == 0 {
                destino = 30
            }else{
                destino = 31
            }
        }else if mes > 7 {
            if mes % 2 == 0 {
                destino = 31
            }else{
                destino = 30
            }
        }
        
        for i in 1...destino {
            arraydias.append(i)
        }
        colection.reloadData()
    }
    
    func checkIfLeapYear(year: Int) -> Int {
        if year % 4 == 0 {
            if year % 100 == 0 {
                if year % 400 == 0 {
                    return 1
                } else {
                    return 0
                }
            } else {
                return 1
            }
        } else {
            return 0
        }
    }
    
    //quando uma celula da CollectionView e clicada, e passado para outra View Controller dados sobre o dia,mes e ano selecionado para o qual sera visualizado ou adicionado eventos
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        diasend = indexPath.row + 1
        performSegue(withIdentifier: "segue1", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue1" {
            if let toViewController = segue.destination as? ViewController3 {
                toViewController.anoS = ano
                toViewController.mesS = mes
                toViewController.diaS = diasend
            }
        }
    }
    
    //funcao prepararCalendario que apenas vai buscar ao array do UserDefaults dados que nos sao uteis acerca do mes e ano selecionado no momento
    func prepararCalendario() {
        ArrayTratamento.removeAll()
        if ArrayEventosPoderoso.count > 0 {
            var i = 0
            while i < ArrayEventosPoderoso.count-1 {
                if ArrayEventosPoderoso[i] == String(ano) && ArrayEventosPoderoso[i+1] == String(mes) {
                            ArrayTratamento.append((ano: Int(ArrayEventosPoderoso[i])!,mes: Int(ArrayEventosPoderoso[i+1])!,dia: Int(ArrayEventosPoderoso[i+2])!, hora: Int(ArrayEventosPoderoso[i+3])!,minutos: Int(ArrayEventosPoderoso[i+4])!,desc: ArrayEventosPoderoso[i+5],photo: ArrayEventosPoderoso[i+6]))
                }
                i = i + 7
            }
        }
    }
    
    //funcao OVERRIDED para recarregar os dados quando o utilizador volta depois de adicionar um evento novo ou limpa os dados nas opcoes para atualizar o calendario com os checkeds
    override func viewDidAppear(_ animated: Bool) {
        ArrayEventosPoderoso = defaults.array(forKey: "eventos")  as? [String] ?? [String]()
        prepararCalendario()
        colection.reloadData()
    }
    
}

