//
//  ViewController5.swift
//  WeatherOrganizer
//
//  Created by PowerX56 on 7/30/17.
//  Copyright © 2017 PowerX56. All rights reserved.
//

import UIKit

//as opcoes permitem a alteracao da cidade para ser apresentado a temperatura e limpar os dados dos eventos
//contem materia TextFieldDelegate
class ViewController5: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textcity: UITextField!
    var ArrayEventosPoderoso : [String] = []
    let defaults = UserDefaults.standard
    var cidade : String = ""
    
    //carrega os dados
    override func viewDidLoad() {
        super.viewDidLoad()
        ArrayEventosPoderoso = defaults.array(forKey: "eventos")  as? [String] ?? [String]()
        cidade =  defaults.string(forKey: "cidade")!
        textcity.text = cidade
    }
    
    //se o utilizador escreveu outra cidade na caixa de texto, atualiza o user Defaults na chave cidade com o nome da nova cidade
    @IBAction func changelocalizacao(_ sender: Any) {
        if textcity.text != "" {
            defaults.set(textcity.text, forKey: "cidade")
        }
    }

    //caso o utilizador clique fora desaparece o teclado
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //caso o utilizador clique em retorno no teclado, desaparece o teclado e atualiza a cidade
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        if textcity.text != "" {
            defaults.set(textcity.text, forKey: "cidade")
        }
        return false;
    }
    
    //limpa os dados do array e guarda no userDefaults, como se trata de remover dados, e pedido ao utilizador uma confirmacao
    @IBAction func limpareventos(_ sender: Any) {
        let refreshAlert = UIAlertController(title: "Confirmacao", message: "Tem a certeza que quer apagar todos os dados? ", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Confirmar", style: .default, handler: { (action: UIAlertAction!) in
            self.ArrayEventosPoderoso.removeAll()
            self.defaults.set(self.ArrayEventosPoderoso, forKey: "eventos")
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: { (action: UIAlertAction!) in
            
        }))
        
        present(refreshAlert, animated: true, completion: nil)
    }
    
}
