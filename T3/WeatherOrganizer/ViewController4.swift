//
//  ViewController4.swift
//  WeatherOrganizer
//
//  Created by PowerX56 on 7/30/17.
//  Copyright © 2017 PowerX56. All rights reserved.
//

import UIKit

//contem materia TextFieldDelegate
class ViewController4: UIViewController, UITextFieldDelegate {

    //recebe os dados enviados da ViewController Anterior
    @IBOutlet weak var TrashButton: UIBarButtonItem!
    
    @IBOutlet weak var ButtonADD: UIButton!
    var TipoAccao : Int = 0
    var anoS2 : Int = 0
    var mesS2 : Int = 0
    var diaS2 : Int = 0
    var ArrayEventosPoderoso : [String] = []
    let defaults = UserDefaults.standard
    var ImStartingToGetItLel : Int = 0
    
    @IBOutlet weak var titulo2: UINavigationItem!
    @IBOutlet weak var buttoneventicone: UIButton!
    @IBOutlet weak var textnome: UITextField!
    @IBOutlet weak var data: UIDatePicker!
    
    //Atualizacao dos dados e do titulo
    override func viewDidLoad() {
        super.viewDidLoad()


        ArrayEventosPoderoso = defaults.array(forKey: "eventos")  as? [String] ?? [String]()
        
        if TipoAccao == 0 {
            titulo2.title = String(diaS2) + "/" + String(mesS2) + "/" + String(anoS2) + " - Criar Evento";
            TrashButton.isEnabled = false
            ButtonADD.setTitle("Adicionar Evento", for: .normal)
        }else if TipoAccao == 1 {
            titulo2.title = String(diaS2) + "/" + String(mesS2) + "/" + String(anoS2) + " - Evento";
            TrashButton.isEnabled = true
            ButtonADD.setTitle("Alterar Evento", for: .normal)
            textnome.text = ArrayEventosPoderoso[ImStartingToGetItLel+5]
            buttoneventicone.setTitle(ArrayEventosPoderoso[ImStartingToGetItLel+6], for: .normal)
            
            let HoraModify : Int =  Int(ArrayEventosPoderoso[ImStartingToGetItLel+3])!
            let MinutoModify : Int =  Int(ArrayEventosPoderoso[ImStartingToGetItLel+4])!
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat =  "HH:mm"
            let date = dateFormatter.date(from: String(HoraModify) + ":" + String(MinutoModify))
            data.date = date!
            
        }
    }

    //o botao Back apenas fecha a Modal View
    @IBAction func backbutton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //caso o utilizador queira remover um evento, e feito um ciclo for que vai movimentando ao contrario para remover certas posicoes do array para eliminar o evento 
    @IBAction func TrashButtonClick(_ sender: Any) {
        let refreshAlert = UIAlertController(title: "Confirmacao", message: "Tem a certeza que quer apagar o evento? ", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Confirmar", style: .default, handler: { (action: UIAlertAction!) in
            for index in stride(from: 6, to: -1, by: -1) {
                self.ArrayEventosPoderoso.remove(at: self.ImStartingToGetItLel+index)
            }
            self.defaults.set(self.ArrayEventosPoderoso, forKey: "eventos")
            self.dismiss(animated: true, completion: nil)
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: { (action: UIAlertAction!) in
            
        }))
        
        present(refreshAlert, animated: true, completion: nil)
    }
    //Quando se clica no botao do tema do evento, e alterado o texto para dar hipotese do utilizador referir que tipo de evento se trata, isto ira influenciar o icone do evento
    @IBAction func simbolobutton(_ sender: Any) {
        if buttoneventicone.titleLabel?.text == "Evento Normal" {
            buttoneventicone.setTitle("Restaurante", for: .normal)
        }else if buttoneventicone.titleLabel?.text == "Restaurante" {
            buttoneventicone.setTitle("Desporto", for: .normal)
        }else if buttoneventicone.titleLabel?.text == "Desporto" {
            buttoneventicone.setTitle("Festa", for: .normal)
        }else if buttoneventicone.titleLabel?.text == "Festa" {
            buttoneventicone.setTitle("Evento Normal", for: .normal)
        }
    }
    
    //caso o utilizador clique fora desaparece o teclado
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //botao que adiciona o evento novo,e tudo adicionado no mesmo array, que depois e feito o tratamento dos dados nas outras Views atravez do ArrayTratamento que anda de 7 em 7 casas preenchendo os tipos de dados respetivos, tambem responsavel por editar um evento conforme o tipo de accao
    @IBAction func addeventbutton(_ sender: Any) {
        
        if textnome.text != "" {
            data.datePickerMode = .time
            let date = data.date
            let components = Calendar.current.dateComponents([.hour, .minute], from: date)
            let horaS2 = components.hour!
            let minutosS2 = components.minute!
            
            if TipoAccao == 0 {
                ArrayEventosPoderoso.append(String(anoS2))
                ArrayEventosPoderoso.append(String(mesS2))
                ArrayEventosPoderoso.append(String(diaS2))
                
                ArrayEventosPoderoso.append(String(horaS2))
                ArrayEventosPoderoso.append(String(minutosS2))
                ArrayEventosPoderoso.append(textnome.text!)
                ArrayEventosPoderoso.append((buttoneventicone.titleLabel?.text)!)
            }else if TipoAccao == 1 {
                ArrayEventosPoderoso[ImStartingToGetItLel+3] = String(horaS2)
                ArrayEventosPoderoso[ImStartingToGetItLel+4] = String(minutosS2)
                ArrayEventosPoderoso[ImStartingToGetItLel+5] = textnome.text!
                ArrayEventosPoderoso[ImStartingToGetItLel+6] = (buttoneventicone.titleLabel?.text)!
            }
            defaults.set(ArrayEventosPoderoso, forKey: "eventos")
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
}
