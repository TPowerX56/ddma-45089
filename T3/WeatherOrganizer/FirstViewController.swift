//
//  FirstViewController.swift
//  WeatherOrganizer
//
//  Created by PowerX56 on 7/29/17.
//  Copyright © 2017 PowerX56. All rights reserved.
//

import UIKit
import CoreLocation
import Foundation

// classe utilizada para a tableView
class ControloMenu: UITableViewCell {
    @IBOutlet weak var Texto2: UILabel!
    @IBOutlet weak var Texto1: UILabel!
    @IBOutlet weak var ImagemAuxiliar: UIImageView!
}

class FirstViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {

    //tabela TableView clock Label Relogio date Label do dia de hoje
    @IBOutlet weak var tabela: UITableView!
    @IBOutlet weak var clock: UILabel!
    @IBOutlet weak var date: UILabel!
    //array com informacao do tempo
    var InformacaoTempo = [Weather]()
    //nosso array de dados nao organizado guardado no user defaults
    var ArrayEventosPoderoso : [String] = []
    //array tuplo que vai tratar e utilizar os dados
    var ArrayTratamento:[(ano: Int, mes: Int, dia: Int, hora: Int, minutos: Int, desc: String, photo: String)] = []
    //variaveis NOW que contem a informacao do tempo atual utilizado para fazer comparacoes com os eventos que existem, para saber qual e o mais proximo para ser apresentado na tableView
    var AnoNOW : Int = 0
    var mesNOW : Int = 0
    var diaNOW : Int = 0
    var horaNOW : Int = 0
    var minutoNOW : Int = 0
    //e criado um timer para atualizar a tableview e a label de hora atual a cada minuto
    weak var timer2: Timer?
    //variavel cidade utilizada para saber a que respetiva cidade se deve obter a data sobre o tempo, personalizavel no separador opcoes
    var cidade : String = ""
    
    //materia user Defaults
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        seccao()
        reloadTHEGUN()
        AtualizarTempo(location: cidade)
    }
    
    //funcao que trata da atualizacao do tempo atual e das variaveis de tempo
    func relogio() {
        let dated = Date()
        let calendar = Calendar.current
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        
        AnoNOW = calendar.component(.year, from: dated)
        mesNOW = calendar.component(.month, from: dated)
        diaNOW = calendar.component(.day, from: dated)
        horaNOW = calendar.component(.hour, from: dated)
        minutoNOW = calendar.component(.minute, from: dated)
        
        var str = "\(String(calendar.component(.minute, from: dated)))"
        if (calendar.component(.minute, from: dated) < 10) { str = "0\(String(calendar.component(.minute, from: dated)))" }
        
        clock.text = String(calendar.component(.hour, from: dated)) + ":" + str
        date.text = String(formatter.string(from: dated))
        
        if let validado = defaults.string(forKey: "cidade") {
            cidade = validado
        }else{
            cidade = "Lisboa"
            defaults.set(cidade, forKey: "cidade")
        }
    }

    //procedimento responsavel por dar reload a cada 60 segundos para apresentar o relogio sempre atualizado e se ainda o evento Proximo se encontra acima da nossa data
    func reloadTHEGUN() {
            timer2 = Timer.scheduledTimer(withTimeInterval: 60.0, repeats: true) { [weak self] _ in
                self?.relogio()
                self?.ArrayEventosPoderoso = self?.defaults.array(forKey: "eventos")  as? [String] ?? [String]()
                self?.prepararCalendario()
                self?.tabela.reloadData()
        }
    }
    
    //procedimento responsavel pela a obtencao da informacao sobre o tempo utilizando a estrutura do ficheiro tempoAPI.swift faz uma request ao site, e e devolvido a informacao em formato JSON
    func AtualizarTempo(location:String) {
        CLGeocoder().geocodeAddressString(location) { (placemarks:[CLPlacemark]?, error:Error?) in
            if error == nil {
                if let location = placemarks?.first?.location {
                    Weather.forecast(withLocation: location.coordinate, completion: { (results:[Weather]?) in
                        if let weatherData = results {
                            self.InformacaoTempo = weatherData
                            DispatchQueue.main.async {
                                self.tabela.reloadData()
                            }
                        }
                    })
                }
            }
        }
    }
    
    //a TableView do menu principal e constituido por 4 linhas, logo o retorno de numero de linhas utilizadas e sempre 4
    //uma para apenas dizer Tempo para Hoje
    //A seguinte apresenta o tempo de Hoje obtido no DarkSky como a imagem respetiva, e a nossa localizacao
    //asseguir apresenta outra introducao proximo evento
    //e por fim apresenta o evento mais proximo que temos no calendario
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ControloMenu
        
        if InformacaoTempo.count > 0 {
            let TempoHoje = InformacaoTempo[0]
            
            if indexPath.row == 0 {
                cell.Texto1.text = "Tempo para Hoje:"
                cell.Texto2.text = ""
            }else if indexPath.row == 1 {
                cell.ImagemAuxiliar.image = UIImage(named: TempoHoje.icon)
                cell.Texto1.text = cidade
                let x : Double = (TempoHoje.temperature - 32) * (5/9)
                cell.Texto2.text = String(Double(round(10*x)/10))  + "ºC"
            }else if indexPath.row == 2 {
                cell.Texto1.text = "Proximo Evento:"
                cell.Texto2.text = ""
            }else if indexPath.row == 3 {
                if ArrayTratamento.count > 0 {
                    cell.Texto1.text = ArrayTratamento[0].desc
                 //caso a variavel dos minutos tenha apenas 1 digito, e colocado outro antes para melhor aparencia
                    var minutosFix2 : String = ""
                    let minutosFix : Int = ArrayTratamento[0].minutos
                    if minutosFix < 10 {
                        minutosFix2 = "0" + String(minutosFix)
                    }else{
                        minutosFix2 = String(minutosFix)
                    }
                    
                    //ja sabemos que a posicao 0 pertence ao evento mais proximo porque foi feito o ordenamento, depois do tratamento dos dados na funcao prepararCalendario
                    cell.Texto2.text = String(ArrayTratamento[0].dia) + "/" + String(ArrayTratamento[0].mes) +
                     "/" + String(ArrayTratamento[0].ano) + " " + String(ArrayTratamento[0].hora) + ":" + minutosFix2
                    cell.ImagemAuxiliar.image = UIImage(named: ArrayTratamento[0].photo)
                }else{
                    cell.Texto1.text = "Sem Evento Proximo!"
                    cell.Texto2.text = ""
                    cell.ImagemAuxiliar.image = nil
                }
            }
        }else{
            if indexPath.row == 0 {
                cell.Texto1.text = "Conectando a internet..."
                cell.Texto2.text = ""
                cell.ImagemAuxiliar.image = nil
            }else if indexPath.row == 1 {
                cell.Texto1.text = ""
                cell.Texto2.text = ""
                cell.ImagemAuxiliar.image = nil
            }
        }
        return cell
    }
    
    //funcao que pega no ArrayEventosPoderoso que contem a informacao do UserDefaults e coloca no ArrayTratamento, com um conjunto de condicoes para apenas introduzir os eventos que sao superiores a hora e data atual que estejam no mesmo ano
    func prepararCalendario() {
        ArrayTratamento.removeAll()
        if ArrayEventosPoderoso.count > 0 {
            var i = 0
            while i < ArrayEventosPoderoso.count-1 {
                if Int(ArrayEventosPoderoso[i])! >= AnoNOW && Int(ArrayEventosPoderoso[i+1])! >= mesNOW && Int(ArrayEventosPoderoso[i+2])! >= diaNOW {
                            if Int(ArrayEventosPoderoso[i+3])! > horaNOW {
                                    ArrayTratamento.append((ano: Int(ArrayEventosPoderoso[i])!,mes: Int(ArrayEventosPoderoso[i+1])!,dia: Int(ArrayEventosPoderoso[i+2])!, hora: Int(ArrayEventosPoderoso[i+3])!,minutos: Int(ArrayEventosPoderoso[i+4])!,desc: ArrayEventosPoderoso[i+5],photo: ArrayEventosPoderoso[i+6]))
                            }else if Int(ArrayEventosPoderoso[i+3])! == horaNOW {
                                if Int(ArrayEventosPoderoso[i+4])! >= minutoNOW {
                                ArrayTratamento.append((ano: Int(ArrayEventosPoderoso[i])!,mes: Int(ArrayEventosPoderoso[i+1])!,dia: Int(ArrayEventosPoderoso[i+2])!, hora: Int(ArrayEventosPoderoso[i+3])!,minutos: Int(ArrayEventosPoderoso[i+4])!,desc: ArrayEventosPoderoso[i+5],photo: ArrayEventosPoderoso[i+6]))
                                }
                            }
                }
                i = i + 7
            }
            //ordena o nosso array e em caso de condicoes duplas, por exemplo o dia ser igual, ordenar pela hora e pelos os minutos de modo a obter o nosso array de eventos ordenado do mais antigo para o mais recente
            ArrayTratamento.sort{
                if $0.0 == $1.0{
                    if $0.1 == $1.1{
                        if $0.2 == $1.2{
                            if $0.3 == $1.3{
                                return $0.4 < $1.4
                            }else{
                                return $0.3 < $1.3
                            }
                        }else{
                            return $0.2 < $1.2
                        }
                    }else{
                        return $0.1 < $1.1
                    }
                }else{
                    return $0.0 < $1.0
                }
            }

        }
    }
    
    //funcoes OVERRIDED para recarregar os dados caso o utilizador passe de um tab para outro, ou volte de um Modal
    override func viewDidAppear(_ animated: Bool) {
        seccao()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        seccao()
    }
    
    func seccao() {
        InformacaoTempo.removeAll()
        relogio()
        ArrayEventosPoderoso = defaults.array(forKey: "eventos")  as? [String] ?? [String]()
        prepararCalendario()
        AtualizarTempo(location: cidade)
        tabela.reloadData()
    }
    
}

