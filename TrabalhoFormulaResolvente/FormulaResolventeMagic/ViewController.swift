import UIKit

extension Double {
    /// Extensao que faz o arredondamento do resultado da formula resolvente
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

class ViewController: UIViewController,UITextFieldDelegate {

    //Variaveis globais de controlos para serem utilizados na funçao de calculo e apresentacao do resultado da formula resolvente
    var TXTA : UITextField!;
    var TXTB : UITextField!;
    var TXTC : UITextField!;
    var labelResultado : UILabel!;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // criaçao de controlos
        var label : UILabel!;
        var label2 : UILabel!;
        var label3 : UILabel!;
        var label4 : UILabel!;

        var Botao : UIButton;
        // posicionamento dos controlos
        label = UILabel(frame: CGRect(x: 25, y: 30, width: (self.view.frame.width-50), height: 30))
        label2 = UILabel(frame: CGRect(x: 10, y: 70, width: 30, height: 30))
        label3 = UILabel(frame: CGRect(x: 10, y: 100, width: (self.view.frame.width-50), height: 30))
        label4 = UILabel(frame: CGRect(x: 10, y: 130, width: (self.view.frame.width-50), height: 30))
        labelResultado = UILabel(frame: CGRect(x: 90, y: 210, width: (self.view.frame.width-50), height: 30))
        
        TXTA = UITextField(frame: CGRect(x: 30, y: 75, width: (self.view.frame.width-50), height: 20))
        TXTB = UITextField(frame: CGRect(x: 30, y: 105, width: (self.view.frame.width-50), height: 20))
        TXTC = UITextField(frame: CGRect(x: 30, y: 135, width: (self.view.frame.width-50), height: 20))
        
        Botao = UIButton(frame: CGRect(x: 100, y: 160, width: 100, height: 40))

        Botao.setTitle("Calcular", for: .normal)
        
        // alinhamento e mudança de texto de labels
        label.textAlignment = NSTextAlignment.center
        label.text = "Formula Resolvente"
        label2.text = "A:"
        label3.text = "B:"
        label4.text = "C:"
        
        //Bordas das textboxes
        TXTA.borderStyle = UITextBorderStyle.bezel
        TXTB.borderStyle = UITextBorderStyle.bezel
        TXTC.borderStyle = UITextBorderStyle.bezel
        Botao.backgroundColor = UIColor.gray
        
        //atribuiçao da açcao ao botao
        Botao.addTarget(self, action: #selector(clique), for: UIControlEvents.touchDown)
        
        //adicionar a subview
        view.addSubview(label)
        view.addSubview(label2)
        view.addSubview(label3)
        view.addSubview(label4)
        view.addSubview(labelResultado)
        view.addSubview(TXTA)
        view.addSubview(TXTB)
        view.addSubview(TXTC)
        view.addSubview(Botao)
        self.TXTA.delegate = self
        self.TXTB.delegate = self
        self.TXTC.delegate = self
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return false;
    }
    
    //Funçao de validacao se é um numero intruduzido
    func NumeroValidacao(string: String) -> Bool {
        return Int(string) != nil
    }
    
    func clique(){
                //funcao recebe os dados das textboxes e executa a funçao da formula resolvente, verificando se ha campos vazios
        if TXTA.text == "" || TXTB.text == "" || TXTC.text == "" {
            labelResultado.text = "Campos Vazios!"
        }else{
            
            if NumeroValidacao(string: TXTA.text!) == true && NumeroValidacao(string: TXTB.text!) == true && NumeroValidacao(string: TXTC.text!) == true {
                let ReceberA : Double = Double(TXTA.text!)!
                let ReceberB : Double = Double(TXTB.text!)!
                let ReceberC : Double = Double(TXTC.text!)!
                
                labelResultado.text = formula(a: ReceberA, b: ReceberB, c: ReceberC)
            } else{
                labelResultado.text = "Numero Inserido Invalido!"
            }
        }
        self.view.endEditing(true)
    }
    
    func formula(a: Double,b: Double,c: Double) -> String {
        // funçao do calculo da formula resolvente
        let auxiliar : Double = 4*a*c
        let auxiliar2 : Double = sqrt(Double((b*b) - auxiliar))
        let formulaResultado : Double = (-b + auxiliar2)/(2*a)
        let formulaResultado2 : Double = (-b - auxiliar2)/(2*a)
        //Verificaçao se resultado impossivel
        if formulaResultado.isNaN == false && formulaResultado2.isNaN == false{
                    return "X= " + String(formulaResultado.roundTo(places: 2)) + " V X= " + String(formulaResultado2.roundTo(places: 2))
        }else{
            return "Impossivel"
        }
    }
}
