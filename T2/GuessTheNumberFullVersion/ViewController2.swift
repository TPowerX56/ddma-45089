//
//  ViewController2.swift
//  GuessTheNumberFullVersion
//
//  Created by Developer on 08/06/17.
//  Copyright © 2017 istec. All rights reserved.
//

import UIKit

class ViewController2: UIViewController, UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{
    
    @IBOutlet var Tentativas: UILabel!
    @IBOutlet var Guess: UILabel!
    @IBOutlet var Tentar: UIButton!
    @IBOutlet var NumeroTXT: UITextField!
    @IBOutlet weak var Tabela: UITableView!
    
    var HistoricoArray = ["Historico de Jogadas"]
    var NumeroSecreto = 0
    var TentativasUsadas : Int = 0
    var nick : String = ""
    var EspelhoArray : [String] = ["Historico de Jogadas"]
    var dificuldadeNOW : Int = 0
    
    //no inicio da ativididade, a label informacao e escondida, e a sua fonte e adaptada conforme o texto inserido para caber sempre o texto todo, e conforme a dificuldade que foi escolhida anteriormente e recebida por esta viewcontroller, e determinado o range do randomize da variavel que ira ser o numero secreto a ser adivinhado
    override func viewDidLoad() {
        super.viewDidLoad()
        Guess.isHidden = true
        Guess.adjustsFontSizeToFitWidth = true
        if dificuldadeNOW == 0 {
            NumeroSecreto = Int(arc4random_uniform(100) + 1)
        }else if dificuldadeNOW == 1 {
            NumeroSecreto = Int(arc4random_uniform(500) + 1)
        }else if dificuldadeNOW == 2 {
            NumeroSecreto = Int(arc4random_uniform(1000) + 1)
        }
    }
    
    //codigo da materia teclado delegate textfield
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        RetornoOuClique()
        return false;
    }
    
    //codigo obrigatorio para a tableViewDataSource e tableViewDelegate
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return EspelhoArray.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        var cell:UITableViewCell?
            cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
            cell?.textLabel?.text = EspelhoArray[indexPath.row]
        return cell!
    }
    
    //codigo do botao tentar acertar no valor,sempre com uma label indicativa ao utilizador da informaçao que ocorreu, caso acerte passara para a atividade do resultado levando com ele, o nome, as tentativas que foram utilizadas, o numero que adivinhou e a dificuldade a que estava a jogar para enviar para o highscore na proxima atividade
    @IBAction func TentarClique(_ sender: Any) {
        RetornoOuClique()
    }
    
    func RetornoOuClique() {
        Guess.isHidden = false
        if let tentativa : Int = Int(NumeroTXT.text!) {
            if tentativa > NumeroSecreto {
                Guess.text = "Errado! Numero esta abaixo do referido!"
                BoostTentativas()
            }else if tentativa < NumeroSecreto{
                Guess.text = "Errado! Numero esta Acima do referido!"
                BoostTentativas()
            }else if tentativa == NumeroSecreto{
                Guess.text = "Adivinhou o numero!"
                self.performSegue(withIdentifier: "Novo2", sender: self)
            }
        }else{
            Guess.text = "Numero Invalido!"
        }
    }
    //funcao caso o utilizador falhe o guess the number, ja que o codigo repete duas vezes, foi criada uma funçao, tambem e nesta funcao que atualiza a tabela historico,e criado uma tabela de tratamento de dados chamdo EspelhoArray que absorve os dados que sao adicionados no array original, remove a linha principal que diz "historico de jogadas" na posicao 0, ordena o array para mostrar sempre a jogada mais recente no topo, volta a inserir o titulo Historico de jogadas e atualiza a tableview para o utilizador
    func BoostTentativas() {
        TentativasUsadas = TentativasUsadas + 1
        Tentativas.text = "Numero de tentativas: " + String(TentativasUsadas)
        HistoricoArray.append("Nº tentativa: " + String(TentativasUsadas) + " Numero: " + String(describing: NumeroTXT.text!))
        NumeroTXT.text = ""
        // Update Table Data
        EspelhoArray = HistoricoArray
        
        
        EspelhoArray.remove(at: 0)
        EspelhoArray.reverse()
        insertElementAtIndex(element: "Historico de Jogadas", index: 0)
        self.Tabela.reloadData()
        
    }
    
    //funcao prepare para segue, que transmite variaveis
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Novo2" {
            if let toViewController = segue.destination as? ViewController3 {
                toViewController.NickNow = nick
                toViewController.JogadasNow = TentativasUsadas
                toViewController.NumeroSecretoNow = NumeroSecreto
                toViewController.DificuldadeFinal = dificuldadeNOW
            }
        }
    }
    
    //funcao que insere o elemento na posicao X empurrando os existentes para a frente
    func insertElementAtIndex(element: String?, index: Int) {
        
        while EspelhoArray.count < index + 1 {
            EspelhoArray.append("")
        }
        EspelhoArray.insert(element!, at: index)
    }
}
