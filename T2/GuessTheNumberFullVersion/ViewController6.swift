//
//  ViewController6.swift
//  GuessTheNumberFullVersion
//
//  Created by PowerX56 on 6/20/17.
//  Copyright © 2017 istec. All rights reserved.
//

import UIKit

class ViewController6: UIViewController {

    var level : Int = 1
    var nickRPG : String = ""
    @IBOutlet weak var DESC: UILabel!
    var VidaPause : Int = 100
    var MoneyPause : Int = 0
    
    //ViewController que faz sempre uma breve introduçao ao novo nivel,pode receber os dados quer de tras quando inicia um novo jogo, ou quando acaba um nivel, pois o objetivo e sempre ir iniciar o jogo
    override func viewDidLoad() {
        super.viewDidLoad()

        DESC.text = "LEVEL " + String(level) + " (1-" + String(level*10) + ")"
        //espera um bocado para o utilizador se preparar, e transporta os dados para o jogo
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
            self.performSegue(withIdentifier: "Novo8", sender: self)
        })
        
    }

    //responsavel por transporte de dados
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Novo8" {
            if let toViewController = segue.destination as? ViewController7 {
                toViewController.nickRPGAction = nickRPG
                toViewController.LevelAction = level
                toViewController.Vida = VidaPause
                toViewController.Money = MoneyPause
            }
        }
    }
    
}
