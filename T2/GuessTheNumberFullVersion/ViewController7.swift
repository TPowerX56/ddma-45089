//
//  ViewController7.swift
//  GuessTheNumberFullVersion
//
//  Created by PowerX56 on 6/20/17.
//  Copyright © 2017 istec. All rights reserved.
//

import UIKit

class ViewController7: UIViewController,UITextFieldDelegate {

    //variaveis globais
    var nickRPGAction : String = ""
    var Vida : Int = 100
    var Money : Int = 0
    var NumeroSecreto : Int = 0
    var LevelAction : Int = 0
    var IlusionNewEnemy : Int = 0
    
    //controlos
    @IBOutlet weak var TXTLIFE: UILabel!
    @IBOutlet weak var TXTMONEY: UILabel!
    @IBOutlet weak var HERO: UIImageView!
    @IBOutlet weak var FIREBALL: UIImageView!
    @IBOutlet weak var ENEMY: UIImageView!
    @IBOutlet weak var TXTdesc: UILabel!
    @IBOutlet weak var TXTLEVEL: UILabel!
    
    @IBOutlet weak var BTTATTACK: UIButton!
    @IBOutlet weak var BTTPOTION: UIButton!
    @IBOutlet weak var BTTEXIT: UIButton!
    
    @IBOutlet weak var TXTGUESS: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        TXTdesc.isHidden = true
        FIREBALL.isHidden = true
        TXTdesc.adjustsFontSizeToFitWidth = true
        let maxHit : Int = LevelAction * 10
        
        //numero secreto conforme o nivel que estiver
        NumeroSecreto = Int(arc4random_uniform(UInt32(maxHit)))
        NumeroSecreto = NumeroSecreto + 1
        
        IlusionNewEnemy = Int(arc4random_uniform(3) + 1)
        
        //o inimigo novo e apenas um ilusao escolhida ao acaso entre 3 oponentes possiveis
        if IlusionNewEnemy == 1 {
            ENEMY.image = #imageLiteral(resourceName: "M1")
        }else if IlusionNewEnemy == 2 {
            ENEMY.image = #imageLiteral(resourceName: "M2")
        }else if IlusionNewEnemy == 3 {
            ENEMY.image = #imageLiteral(resourceName: "M3")
        }
        //atualiza a Interface
        TXTLEVEL.text = "LEVEL " + String(LevelAction) + " (1-" + String(LevelAction*10) + ")"
        //GIVE ME THE MONEY and life...
        UpdateGUI()
        
    }
    
    //materia de teclado delegate textfield
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return false;
    }
    
    func UpdateGUI() {
        TXTLIFE.text = String(Vida)
        TXTMONEY.text = String(Money)
    }
    
    //botao responsavel pelo o ATAQUE
    @IBAction func AttackClique(_ sender: Any) {
        if let tentativa : Int = Int(TXTGUESS.text!) {
            if tentativa > NumeroSecreto {
                Errado(entao: "abaixo")
            }else if tentativa < NumeroSecreto{
                Errado(entao: "acima")
            }else if tentativa == NumeroSecreto{
                //se acertou no numero, varios controlos serao colocado como visiveis ou invisiveis
                TXTGUESS.text = ""
                FIREBALL.isHidden = false
                BTTATTACK.isHidden = true
                BTTPOTION.isHidden = true
                BTTEXIT.isHidden = true
                FIREBALL.image=#imageLiteral(resourceName: "FireBall_Right")
                //a bola de fogo passa a ir em dirrecao ao inimigo
                let lucro : Int = Int(arc4random_uniform(50) + 10)
                Money = Money + lucro
                //e gerado lucro ao utilizador que pode ser utilizado para comprar pocoes, gerado random entre 10 e 50
                info(texto: "Vitoria! Ganhaste " + String(lucro) + " Moedas!")
                UpdateGUI()
                //coloca o inimigo em uma posicao derrotado
                if IlusionNewEnemy == 1 {
                    ENEMY.image = #imageLiteral(resourceName: "M1_Dead")
                }else if IlusionNewEnemy == 2 {
                    ENEMY.image = #imageLiteral(resourceName: "M2_Dead")
                }else if IlusionNewEnemy == 3 {
                    ENEMY.image = #imageLiteral(resourceName: "M3_Dead")
                }
                //aumenta o nivel e segue
                LevelAction = LevelAction + 1
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
                    self.performSegue(withIdentifier: "Novo9", sender: self)
                })
            }
        }else{
            info(texto: "Numero Invalido!")
        }
    }

    //botao simples que aumenta a vida retirando dinheiro
    @IBAction func PotionClique(_ sender: Any) {
        if Money >= 30 {
            if Vida != 100 {
                Vida = Vida + 30
                if Vida > 100 {
                    Vida = 100
                }
                Money = Money - 30
                UpdateGUI()
                info(texto: "Vida Restaurada!")
            }else{
                info(texto: "Vida Cheia!")
            }
        }else {
            info(texto: "Dinheiro Insuficiente!")
        }
    }
    
    //Utilizado em varios sitios diferentes no codigo, funcao importante que mostra informacao ao utilizador conforme o argumento, e que desaparece passado 2 segundos
    func info(texto : String) {
        TXTdesc.isHidden = false
        TXTdesc.text = texto
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
            self.TXTdesc.isHidden = true
        })
    }
    
    //funcao casso erra no numero, mais uma vez, utilizado para falhas do tipo superior ou inferior ao numero inserido, em que essa informacao e recebida como argumento
    func Errado(entao : String) {
        TXTGUESS.text = ""
        FIREBALL.isHidden = false
        BTTATTACK.isHidden = true
        BTTPOTION.isHidden = true
        BTTEXIT.isHidden = true
        //alteracao da visibilidade de controlos
        FIREBALL.image = #imageLiteral(resourceName: "FireBall_Left")
        //atualizacao da imagem da bola de fogo para o sentido do utilizador
        let dano : Int = LevelAction + Int(arc4random_uniform(3) + 1)
        //o dano infrigido ao utilizador e sempre o nivel em que se encontra mais um random de 1 a 3
        Vida = Vida - dano
        if Vida > 0 {
            info(texto: "Numero esta " + entao + ". Levaste " + String(dano) + " dano!")
            UpdateGUI()
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
                self.FIREBALL.isHidden = true
                self.BTTATTACK.isHidden = false
                self.BTTPOTION.isHidden = false
                self.BTTEXIT.isHidden = false
            })
        }else if Vida <= 0 {
            //GAME OVER, o utilizador é apresentado de maneira diferente (imagem) e é transportado para o formulario de resultados, o mesmo utilizado no jogo normal, a outra viewController consegue perceber-se que esta a receber resultados a partir da atividade de um jogo de RPG da variavel TipoJogo.
            Vida = 0
            HERO.image = #imageLiteral(resourceName: "HERODEAD")
            UpdateGUI()
            info(texto: "Game Over!")
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
                self.performSegue(withIdentifier: "Novo10", sender: self)
            })
        }
    }
    
    //responsavel por envio de informacao, quer para um novo nivel ou GAMEOVER
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Novo9" {
            if let toViewController = segue.destination as? ViewController6 {
                toViewController.nickRPG = nickRPGAction
                toViewController.level = LevelAction
                toViewController.VidaPause = Vida
                toViewController.MoneyPause = Money
            }
        }else if segue.identifier == "Novo10" {
            if let toViewController = segue.destination as? ViewController3 {
                toViewController.NickNow = nickRPGAction
                toViewController.JogadasNow = LevelAction
                toViewController.TipoJogo = 1
                toViewController.DificuldadeFinal = 3
                toViewController.NumeroSecretoNow = NumeroSecreto
            }
        }
    }
    
}
