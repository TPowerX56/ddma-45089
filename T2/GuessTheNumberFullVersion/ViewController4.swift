//
//  ViewController4.swift
//  GuessTheNumberFullVersion
//
//  Created by PowerX56 on 6/19/17.
//  Copyright © 2017 istec. All rights reserved.
//

import UIKit

//classe necessaria para apresentar varios objetos na tableview cell conforme materia de sala de aula
class cellRanking: UITableViewCell{

    @IBOutlet weak var Photo: UIImageView!
    @IBOutlet weak var Nome: UILabel!
    @IBOutlet weak var Jogadas: UILabel!

}

class ViewController4: UIViewController, UITableViewDelegate,UITableViewDataSource {

    let defaults = UserDefaults.standard
    var arrayResultados : [String] = []
    var ArrayRanking:[(Nome: String, Jogadas: Int)] = []
    var DificuldadeRanking = 0
    @IBOutlet weak var TXTrank: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //carrega os dados conforme a dificuldade que recebeu de alguma atividade anterior
        if DificuldadeRanking == 0 {
                 arrayResultados = defaults.array(forKey: "Ranking")  as? [String] ?? [String]()
                TXTrank.text = "Ranking - Fácil"
        }else if DificuldadeRanking == 1 {
                 arrayResultados = defaults.array(forKey: "Ranking2")  as? [String] ?? [String]()
             TXTrank.text = "Ranking - Médio"
        }else if DificuldadeRanking == 2 {
                 arrayResultados = defaults.array(forKey: "Ranking3")  as? [String] ?? [String]()
                TXTrank.text = "Ranking - Dificíl"
        }else if DificuldadeRanking == 3 {
                arrayResultados = defaults.array(forKey: "Ranking4")  as? [String] ?? [String]()
                TXTrank.text = "Ranking - RPG"
        }
        
        //a partir daqui os dados sao carregados da mesma maneira, pois a estrutura dos dados de todas as dificuldades sao da mesma maneira
        //quando recebe a informacao do unico array responsavel por cada dificuldade, ele sabe que a primeira posicao e sempre o nome e a segunda e a sua pontuacao em jogadas, portanto e criado um ciclo em que anda 2 em 2 passos, em que o primeiro item é o nome e o segundo a sua pontuacao e assim adiciona ao tuplo
        if arrayResultados.count > 0 {
            var i = 0
            while i < arrayResultados.count-1 {
                ArrayRanking.append((Nome: arrayResultados[i], Jogadas: Int(arrayResultados[i+1])!))
                i = i + 2
            }
        }
        //ordena conforme jogo normal ou RPG, pois em caso de RPG o melhor é quem chega ao maior nivel o contrario do jogo normal, em que quem tiver o menor numero de jogadas é o melhor
        if DificuldadeRanking < 3 {
                ArrayRanking.sort {$0.1 < $1.1}
        }else if DificuldadeRanking == 3 {
                ArrayRanking.sort {$0.1 > $1.1}
        }
        
    }
    
    //funcoes obrigatorios da tableView para TableViewDelegate e TableViewDataSource
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ArrayRanking.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellRanking") as! cellRanking
        if DificuldadeRanking < 3 {
                cell.Jogadas.text = "Jogadas: " + String(ArrayRanking[indexPath.row].Jogadas)
        }else if DificuldadeRanking == 3 {
                cell.Jogadas.text = "Nivel: " + String(ArrayRanking[indexPath.row].Jogadas)
        }
        cell.Nome.text = ArrayRanking[indexPath.row].Nome
        //um simples divertimento, se a sua classificacao for entre os 3 primeiros, a sua imagem sera atribuida uma medalha em imagem
        if indexPath.row == 0 {
            cell.Photo.image = #imageLiteral(resourceName: "first")
        }else if indexPath.row == 1 {
            cell.Photo.image = #imageLiteral(resourceName: "second")
        }else if indexPath.row == 2 {
            cell.Photo.image = #imageLiteral(resourceName: "third")
        }else {
            cell.Photo.image = nil
        }
        return cell
    }
}
