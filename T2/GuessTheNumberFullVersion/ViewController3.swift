//
//  ViewController3.swift
//  GuessTheNumberFullVersion
//
//  Created by PowerX56 on 6/17/17.
//  Copyright © 2017 istec. All rights reserved.
//

import UIKit

class ViewController3: UIViewController {

    var NumeroSecretoNow : Int = 0
    var NickNow : String = ""
    var JogadasNow : Int = 0
    var DificuldadeFinal : Int = 0
    var TipoJogo : Int = 0
    
    @IBOutlet weak var BTTRanking: UIButton!
    
    let defaults = UserDefaults.standard
    var arrayResultados : [String] = []
    
    @IBOutlet weak var LBLtitulo: UILabel!
    @IBOutlet weak var NumeroTXT: UILabel!
    @IBOutlet weak var TentativasTXT: UILabel!
    
    //no inicio da viewcontroller, atualiza o texto a ser apresentado caso tenha jogado jogo normal ou RPG
    override func viewDidLoad() {
        super.viewDidLoad()
        if TipoJogo == 0 {
            NumeroTXT.text = "O numero era " + String(NumeroSecretoNow) + "!"
            TentativasTXT.text = "Demorou " + String(JogadasNow) + " Tentativas!"
            LBLtitulo.text = "Acertou!"
        }else if TipoJogo == 1 {
            NumeroTXT.text = "O numero era " + String(NumeroSecretoNow) + "!"
            TentativasTXT.text = "Chegaste ate ao Nivel " + String(JogadasNow) + "!"
            LBLtitulo.text = "Fim do RPG!"
        }
        
        //conforme a dificuldade jogada, ira guardar os dados na respetiva default do seu ranking
        if DificuldadeFinal == 0 {
            arrayResultados = defaults.array(forKey: "Ranking")  as? [String] ?? [String]()
            arrayResultados.append(NickNow)
            arrayResultados.append(String(JogadasNow))
            defaults.set(arrayResultados, forKey: "Ranking")
        }else if DificuldadeFinal == 1 {
            arrayResultados = defaults.array(forKey: "Ranking2")  as? [String] ?? [String]()
            arrayResultados.append(NickNow)
            arrayResultados.append(String(JogadasNow))
            defaults.set(arrayResultados, forKey: "Ranking2")
        }else if DificuldadeFinal == 2 {
            arrayResultados = defaults.array(forKey: "Ranking3")  as? [String] ?? [String]()
            arrayResultados.append(NickNow)
            arrayResultados.append(String(JogadasNow))
            defaults.set(arrayResultados, forKey: "Ranking3")
        }else if DificuldadeFinal == 3 {
            arrayResultados = defaults.array(forKey: "Ranking4")  as? [String] ?? [String]()
            arrayResultados.append(NickNow)
            arrayResultados.append(String(JogadasNow))
            defaults.set(arrayResultados, forKey: "Ranking4")
        }

    }
    
    // caso clique no botao ranking, aproveita a dificuldade que jogou, e vai ja para a viewcontroller do ranking sabendo que dificuldade de ranking quer ver os resultados
    @IBAction func RankingClique(_ sender: Any) {
        self.performSegue(withIdentifier: "Novo6", sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Novo6" {
            if let toViewController = segue.destination as? ViewController4 {
                toViewController.DificuldadeRanking = DificuldadeFinal
            }
        }
    }
    
}
