//
//  ViewController5.swift
//  GuessTheNumberFullVersion
//
//  Created by PowerX56 on 6/19/17.
//  Copyright © 2017 istec. All rights reserved.
//

import UIKit

class ViewController5: UIViewController {

    @IBOutlet weak var FacilBTT: UIButton!
    @IBOutlet weak var MedioBTT: UIButton!
    @IBOutlet weak var DificilBTT: UIButton!
    @IBOutlet weak var RPGBTT: UIButton!
    
    
    var Nome : String = ""
    var Tipo : Int = 0
    var dificuldade : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }

    //butoes com funcoes simples, conforme o botao clicado e enviado o nivel de dificuldade escolhido, se o motivo anteriormente recebido for iniciar um novo jogo ou ir para o ranking
    @IBAction func FacilClique(_ sender: Any) {
        dificuldade = 0
        if Tipo == 0 {
            self.performSegue(withIdentifier: "Novo3", sender: self)
        }else{
            self.performSegue(withIdentifier: "Novo4", sender: self)
        }
    }
    @IBAction func MedioClique(_ sender: Any) {
        dificuldade = 1
        if Tipo == 0 {
            self.performSegue(withIdentifier: "Novo3", sender: self)
        }else{
            self.performSegue(withIdentifier: "Novo4", sender: self)
        }
    }
    @IBAction func DificilClique(_ sender: Any) {
        dificuldade = 2
        if Tipo == 0 {
            self.performSegue(withIdentifier: "Novo3", sender: self)
        }else{
            self.performSegue(withIdentifier: "Novo4", sender: self)
        }
    }
    
    @IBAction func RPGClique(_ sender: Any) {
        dificuldade = 3
        if Tipo == 0 {
            self.performSegue(withIdentifier: "Novo7", sender: self)
        }else{
            self.performSegue(withIdentifier: "Novo4", sender: self)
        }
    }

    //transportaçao de dados
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Novo3" {
            if let toViewController = segue.destination as? ViewController2 {
                toViewController.nick = Nome
                toViewController.dificuldadeNOW = dificuldade
            }
        }else if segue.identifier == "Novo4" {
            if let toViewController = segue.destination as? ViewController4 {
                toViewController.DificuldadeRanking = dificuldade
            }
        }else if segue.identifier == "Novo7" {
            if let toViewController = segue.destination as? ViewController6 {
                toViewController.level = 1
                toViewController.VidaPause = 100
                toViewController.MoneyPause = 0
                toViewController.nickRPG = Nome
            }
        }
    }
    
}
