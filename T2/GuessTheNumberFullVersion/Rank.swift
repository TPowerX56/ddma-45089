//
//  Rank.swift
//  GuessTheNumberFullVersion
//
//  Created by PowerX56 on 6/19/17.
//  Copyright © 2017 istec. All rights reserved.
//

import UIKit

//classe necessaria para a criacao da tabela ranking

class Rank: NSObject {
    var nome = String()
    var fac = String()
    var photo = UIImage()
    
    init (nome:String, fac:String, foto:String) {
        self.nome = nome
        self.fac = fac
        photo = UIImage(named: foto)!
    }
}
