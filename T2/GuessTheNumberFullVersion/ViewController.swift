//
//  ViewController.swift
//  GuessTheNumberFullVersion
//
//  Created by Developer on 08/06/17.
//  Copyright © 2017 istec. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var TXTnome: UITextField!
    @IBOutlet var BTTsair: UIButton!
    @IBOutlet weak var BTTjogar: UIButton!
    
    //variavel que seja alterada para 0 ou 1, 0 caso o utilizador vaia para a actividade seguinte para iniciar um novo jogo, ou 1 se o utilizador for para a proxima atividade para visualizar o ranking, pois a mesma view controller tem varias funçoes
    var TipoQuestaoJogar : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    //codigo acerca do teclado do iphone caso toque fora do ecra ou prima o botao retorno
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        tecladoOuNormal()
        return false;
    }
    
    //janela de confirmacao se quer sair da aplicacao
    @IBAction func SairNow(_ sender: Any) {
        let refreshAlert = UIAlertController(title: "Guess the number", message: "Tem a certeza que deseja sair", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Sim", style: .default, handler: { (action: UIAlertAction!) in
            exit(0)
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: { (action: UIAlertAction!) in
            print("")
        }))
        
        present(refreshAlert, animated: true, completion: nil)
    }
    
    //verifica se o utilizador introduziu um nome de utilizador para jogar, caso a caixa de texto esteja vazia ainda da a possibilidade de jogar como anomino, o nome de utilizador passa a ser automaticamente designado como anomino e quando entra nos highscores, o nome dele sera anomino
    @IBAction func JogarClique(_ sender: Any) {
        tecladoOuNormal()
    }
    
    //o utilizador pode utilizar o botao jogar, como clicando no botao jogar, ou no botao retorno do teclado
    func tecladoOuNormal() {
        self.TipoQuestaoJogar = 0
        if TXTnome.text == "" {
            let refreshAlert2 = UIAlertController(title: "Guess the number", message: "Nome nao introduzido, quer jogar como anomino?", preferredStyle: UIAlertControllerStyle.alert)
            refreshAlert2.addAction(UIAlertAction(title: "Sim", style: .default, handler: { (action: UIAlertAction!) in
                self.TXTnome.text = "Anomino"
                self.performSegue(withIdentifier: "Novo", sender: self)
                self.TXTnome.text = ""
            }))
            
            refreshAlert2.addAction(UIAlertAction(title: "Nao", style: .cancel, handler: { (action: UIAlertAction!) in
                print("")
            }))
            
            present(refreshAlert2, animated: true, completion: nil)
        }else{
            self.performSegue(withIdentifier: "Novo", sender: self)
        }
    }
    
    //envia a variavel tipo jogar como 1, que significa que quer visualizar os rankings
    @IBAction func RankingClique(_ sender: Any) {
        self.TipoQuestaoJogar = 1
        self.TXTnome.text = "Anomino"
        self.performSegue(withIdentifier: "Novo", sender: self)
    }
    
    //codigo responsavel por passar os argumentos para outra view controller
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Novo" {
            if let toViewController = segue.destination as? ViewController5 {
                toViewController.Nome = TXTnome.text!
                toViewController.Tipo = TipoQuestaoJogar
            }
        }
    }
}

