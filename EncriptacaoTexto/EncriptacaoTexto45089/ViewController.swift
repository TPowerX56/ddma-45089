//
//  ViewController.swift
//  EncriptacaoTexto45089
//
//  Created by Tiago Marques on 28/04/2017.
//  Copyright © 2017 Tiago Marques. All rights reserved.
//

import UIKit
// Extensao responsavel por converter o caracter para ASCII CODE
extension Character {
    var asciiValue: UInt32? {
        return String(self).unicodeScalars.filter{$0.isASCII}.first?.value
    }
}

class ViewController: UIViewController {

    @IBOutlet weak var TXT: UITextField!
    @IBOutlet weak var BotaoASCII: UIButton!
    @IBOutlet weak var Resultado: UILabel!
    @IBOutlet weak var BotaoBinaria: UIButton!
    @IBOutlet weak var BotaoOctal: UIButton!
    @IBOutlet weak var TXT2: UITextField!
    @IBOutlet weak var BotaoASCII2: UIButton!
    @IBOutlet weak var BotaoBinaria2: UIButton!
    @IBOutlet weak var BotaoOctal2: UIButton!
    @IBOutlet weak var Resultado2: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Quando inicia a atividade, retira o nome que apresenta ao utilizador que é "label"
        Resultado.text = ""
        Resultado2.text = ""
    }

    @IBAction func Clique(_ sender: Any) {
        //o objetivo deste botao e converter para ascii code, as letras apresentadas
        //com uma ecriptacao especial pois, os numeros depois de convertidos para ASCII
        //sao multiplicados por 2, o primeiro numero ainda é adicionado mais 2 valores
        //de seguida, os numeros de 2 em 2, isto é, o resto de divisao for 0
        // é atribuido 5 valores
        //por fim, o ultimo character quando é convertido, é adicionado 2 valores
        let texto = TXT.text
        var Resultado2 : String = ""
        var controlo : Int = 1
        
        for letra in (texto?.characters)! {
            if var letra2 = letra.asciiValue { //validaçao para nao permitir valores fora da tabela de ASCII
                //todos os valores sao multiplicadosp or 2
                letra2 = letra2 * 2
                // um controlo de IF para verificar se trata de algum dos caracteres especiais para adicionar valores extras
                if controlo == 1 {
                    letra2 = letra2 + 3
                }else if controlo == (texto?.characters.count)!{
                    letra2 = letra2 + 2
                   }else if controlo % 2 == 0{
                    letra2 = letra2 + 5
                    }
                //é adicionado a uma variavel acumuladora, as conversoes e um espaço que divida cada conversao e no final é apresentado o resultado
                    Resultado2 = Resultado2 + String(letra2)
                    Resultado2 = Resultado2 + " "
            }else{
                    Resultado2 = "Caracteres Invalidos!"
                }
            controlo = controlo + 1
        }
        Resultado.text = Resultado2
    }
    
    @IBAction func Clique2(_ sender: Any) {
        //botao responsavel pela conversao para binario
        Resultado.text=Divisao(TextoObtido: TXT.text!,Tipo: 2)
    }
    
    @IBAction func Clique3(_ sender: Any) {
        //botao responsavel pela conversao para octal
        Resultado.text=Divisao(TextoObtido: TXT.text!,Tipo: 8)
    }
    
    func Divisao(TextoObtido : String,Tipo : Int) -> String {
        // como a conversao para binario e octal é o do mesmo algoritmo, a diferenca é apenas o numero a qual se quer converter, foi criado uma funcao com argumentos de entrada
        let texto = TextoObtido
        var Resultado2 : String = ""
        
        for letra in (texto.characters) {
            if let letra2 = letra.asciiValue { //validacao novamente da tabela ASCII
                var letra3 : Int = Int(letra2)
                var letra4 : String = ""
                while letra3 >= Tipo { //este ciclo divide o numero obtendo sempre o resto da divisao para fazer a conversao binaria
                    letra4 = letra4 + String(letra3 % Tipo)
                    letra3 = letra3 / Tipo
                }
                letra4 = letra4 + String(letra3)
                //no final como a conversao para binaria requere juntar os restos da divisao de uma ordem inversa, foi utilizado um metodo reversed que já vem com o SWIFT
                let letra5 = String(letra4.characters.reversed())
                Resultado2 =  Resultado2 + String(letra5)
                Resultado2 = Resultado2 + " "
            }else{
                Resultado2 = "Caracteres Invalidos!"
            }
        }
        return Resultado2
    }
    
    //funcao que transforma o numero em o caracter, com o numero da posicao da tabela ascii como argumento de entrada
    func characterFromInt(index : Int) -> String {
        var characterString = ""
        characterString.append(Character(UnicodeScalar(index)!))
        return characterString
    }

    @IBAction func Clique11(_ sender: Any) {
        //Botao responsavel pela decriptaçao de ASCII com os extras de volta novamente para as letras
        let texto = TXT2.text
        var Resultado3 : String = ""
        // é criado um array em que o delimitador é o espaço e mete os numeros separados dentro do array
        let ArrayTXT = texto!.characters.split{$0 == " "}.map(String.init)
        
        //percorre o array, transformando cada posiçao do array em o caracter de volta, verificando agora em metodo inverso a existencia de caracters especiais, isto e, se é o primeiro, 2 em 2, ou o ultimo para desta vez retirar os valores a mais antes de converter, em seguida é dividido por 2, pois tinhamos multiplicado todos eles quando foi feito a encriptaçao.
        for i in 0...ArrayTXT.count-1 {
            var letra1 : Int = Int(ArrayTXT[i])!
            if i == 0 {
                letra1 = letra1 - 3
            }else if i == ArrayTXT.count-1 {
                letra1 = letra1 - 2
             }else if i % 2 != 0 {
                letra1 = letra1 - 5
            }
            letra1 = letra1 / 2
            let letra2 = characterFromInt(index: letra1)
            Resultado3 = Resultado3 + letra2
        }
        Resultado2.text = Resultado3
    }
    
    @IBAction func Clique21(_ sender: Any) {
        //botao que decripta o texto binario de volta para o texto
        Resultado2.text = Multiplicacao(TextoObtido: TXT2.text!, Tipo: 2)
    }
    
    @IBAction func Clique22(_ sender: Any) {
        //botao que decripta o texto octal de volta para o texto
        Resultado2.text = Multiplicacao(TextoObtido: TXT2.text!, Tipo: 8)
    }
    
    func Multiplicacao(TextoObtido : String,Tipo : Int) -> String {
        //Funcao que converte o numero octal/binario (mais uma vez em forma de funçao) para Decimal para assim poder converter para caracter segundo a tabela ASCII
        let texto = TextoObtido
        var Resultado3 : String = ""
        var letra2 : Int = 0
        var letra3 : String = ""
        var soma : Int = 0
        var i = 0
        var p = 0
        //variaveis iniciais
        //mais uma vez separada os valores em array utilizando o espaço como indicador
        // ex: 141 142 143.....
        let ArrayTXT = texto.characters.split{$0 == " "}.map(String.init)
        
        //desta vez mais complexo pois por cada posicao do array, é necessario percorrer cada numero, para fazer o numero vezes o Tipo de dados (binario ou octal) levantado a posicao que ele esta no numero.
        while i <= ArrayTXT.count-1 {
            soma = 0
            p  = ArrayTXT[i].characters.count-1
            for letra in ArrayTXT[i].characters {
                let letra1 : Int = Int(String(letra))!
                let auxiliar = p
                var auxiliar2 : Int = Tipo
                var o = 0
                if auxiliar>0 {
                    while o < auxiliar-1 {
                        auxiliar2 = auxiliar2 * Tipo
                        o = o + 1
                    }
                    letra2 = letra1 * auxiliar2
                }else{
                    letra2 = letra1
                 }
                soma = soma + letra2
                p = p - 1
            }
            letra3 = characterFromInt(index: soma)
            Resultado3 = Resultado3 + letra3
            i = i + 1
        }
        
        return Resultado3
    }
}

