# Repositório de Programação para Dispositivos Móveis II - IOS #
[email](mailto:tiagom_8@hotmail.com)
## tiagom_8@hotmail.com ##
* Aluno Tiago Marques
* Nº 45089 CTeSP -Desenvolvimento para Dispositivos Móveis - 1º ano

| Trabalhos        | Topico           | URL  |
| ------------- |:-------------:| -----:|
| T1      | Trabalho RGB | [Link](https://bitbucket.org/TPowerX56/ddma-45089/src/5c9becd8829532ebf774da12f172967a22c43e5b/T1/?at=master) |
| T2     | Guess the Number      |   [Link](https://bitbucket.org/TPowerX56/ddma-45089/src/5c9becd8829532ebf774da12f172967a22c43e5b/T2/?at=master) |
| T3 | Weather Organizer      |    [Link](https://bitbucket.org/TPowerX56/ddma-45089/src/5c9becd8829532ebf774da12f172967a22c43e5b/T3/?at=master) |

![ISTEC](http://www.istec.pt/wp-content/uploads/2015/04/logoHorizontal.png)

![DDM](http://www.istec.pt/wp-content/uploads/2015/04/DDM.png)