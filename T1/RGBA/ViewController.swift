//
//  ViewController.swift
//  RGBA
//
//  Created by Tiago Marques on 28/05/2017.
//  Copyright © 2017 TiagoMarques. All rights reserved.
//

import UIKit

//Responsavel por receber os valores do Slider e Devolver a cor respetiva
extension UIColor{
    class func MudarCor(red: Int, green: Int, blue: Int, alpha: Int) -> UIColor{
        let newRed = CGFloat(red)/255
        let newGreen = CGFloat(green)/255
        let newBlue = CGFloat(blue)/255
        let newAlpha = CGFloat(alpha)/100
        return UIColor(red: newRed, green: newGreen ,blue: newBlue , alpha: newAlpha)
    }
}

class ViewController: UIViewController {

    //Criaçao dos Controlos
    var labelColor :UILabel!;
    var labelInfo :UILabel!;
    var SliderR :UISlider!;
    var SliderG :UISlider!;
    var SliderB :UISlider!;
    var SliderA :UISlider!;
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
        //Posicionamento
        labelColor = UILabel(frame: CGRect(x: 25, y: 30, width: (self.view.frame.width-50), height: 150))
        labelInfo = UILabel(frame: CGRect(x: 10, y: 200, width: 300, height: 30))
        SliderR = UISlider(frame: CGRect(x: 10, y: 250, width: 300, height: 30))
        SliderG = UISlider(frame: CGRect(x: 10, y: 300, width: 300, height: 30))
        SliderB = UISlider(frame: CGRect(x: 10, y: 350, width: 300, height: 30))
        SliderA = UISlider(frame: CGRect(x: 10, y: 400, width: 300, height: 30))
        
        //Valores maximos e iniciais dos sliders conforme foto do PDF
        SliderR.maximumValue = 255
        SliderR.value = 159
        SliderG.maximumValue = 255
        SliderG.value = 69
        SliderB.maximumValue = 255
        SliderB.value = 56
        SliderA.maximumValue = 100
        SliderA.value = 76
        
        //Alteraçao da cor da label inicial
        cor()
        
        //Acçao atribuida aos sliders
        SliderR.addTarget(self, action: #selector(cor), for: UIControlEvents.valueChanged)
        SliderG.addTarget(self, action: #selector(cor), for: UIControlEvents.valueChanged)
        SliderB.addTarget(self, action: #selector(cor), for: UIControlEvents.valueChanged)
        SliderA.addTarget(self, action: #selector(cor), for: UIControlEvents.valueChanged)
        
        //Melhorar Aspeto Grafico dos Sliders
        SliderR.tintColor = UIColor.red
        SliderG.tintColor = UIColor.green
        SliderB.tintColor = UIColor.blue
        SliderA.tintColor = UIColor.black
        
        //Adicionar a subView os controlos
        view.addSubview(labelColor)
        view.addSubview(labelInfo)
        view.addSubview(SliderR)
        view.addSubview(SliderG)
        view.addSubview(SliderB)
        view.addSubview(SliderA)
    }

    func cor(){
        //Funçao executada apos o valor do Slider ser alterado,atualizando a cor na label e o texto na label de informaçao
        labelInfo.text = "R: " + String(Int(SliderR.value)) + " G: " + String(Int(SliderG.value)) + " B: " + String(Int(SliderB.value)) + " A: " + String(Int(SliderA.value))
        
        labelColor.backgroundColor = UIColor.MudarCor(red: Int(SliderR.value), green: Int(SliderG.value), blue: Int(SliderB.value), alpha: Int(SliderA.value))
    }
        
}

